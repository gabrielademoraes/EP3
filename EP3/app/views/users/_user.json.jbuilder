json.extract! user, :id, :name, :email, :company_college_school, :phone, :created_at, :updated_at
json.url user_url(user, format: :json)
